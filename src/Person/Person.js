import React from 'react';
import styled from 'styled-components';
// import Radium from 'radium';
import './Person.css'

const StyleDiv = styled.div`
    width:60%;
    text-align:center;
    border: 1px solid #eee;
    margin:15px auto;
    box-shadow: 0px 2px 3px #ccc;
    padding:16px;
    @media(min-width:500px){
        width:450px 
    }
`;
const person = (props) => {
   return(
    <StyleDiv> 
        <p onClick={props.click}>{`Hii, I am ${props.name}. My age is ${props.age}`}</p>
        <p>{props.children}</p>
        <input type="text" onChange={props.changed} value={props.name}/>
    </StyleDiv>
   );
   //<div className='Person'></div>  
}



export default person;