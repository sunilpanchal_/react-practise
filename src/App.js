import React, { Component } from 'react';
// import Radium,{StyleRoot} from 'radium';
import styled from 'styled-components';
import './App.css';
import Person from './Person/Person';

const StyledButton = styled.button`
background-color:${props=>props.alt?'red':'green'};
color:white;
font:inherit;
border:1px solid blue;
padding:8px;
cursor:pointer;

&:hover{
  background-color:${props=>props.alt?'salmon':'lightgreen'};
  color:black;
}`;

class App extends Component{
  state={
    persons:[
      {id:'ddsdkek',name:'Sunil',age:21},
      {id:'erwwf',name:'Ankit',age:23},
      {id:'f34gg',name:'Nitin',age:22}
    ]
  }
  // switchNameHandler=(newName='Sunil')=>{
  //     this.setState({
  //       persons:[
  //         {name:newName,age:18},
  //         {name:'Ankit Panchal',age:20},
  //         {name:'Nitin Panchal',age:19}
  //         ],
  //       showPersons:false
  //     })
  // }

  deleteNameHandler=(personIndex)=>{
    //const person = this.state.persons; //it is accessing the pointer of person object
    // person =this.state.persons.slice(); // it is creating reference of the person object it is more convinent method than upper one
    const person = [...this.state.persons]; //this is the modern way to do upper one is also good
    person.splice(personIndex,1);
    this.setState({persons:person});
  }

  
  namedChangedHandler = (event,id) =>{
    const personIndex = this.state.persons.findIndex(p=>{
      return p.id===id;
    });
    const person = {...this.state.persons[personIndex]};
    person.name=event.target.value;
    const persons=[...this.state.persons];
    persons[personIndex]=person;
  
    this.setState({persons:persons});
  }
  togglePersonsHandler=()=>{
    const doesShow=this.state.showPersons;
    this.setState({showPersons:!doesShow});
  }


  render(){
    let persons = null;
  
    if(this.state.showPersons)
    {
      persons=(
      <div>
      {this.state.persons.map((person,index)=>{
        return <Person name={person.name} 
                       age={person.age}  
                       click={()=>this.deleteNameHandler(index)}
                       key={person.id}
                       changed={(event)=>this.namedChangedHandler(event,person.id)}/>
      })}
      {/* <Person 
      name={this.state.persons[0].name} 
      age={this.state.persons[0].age}
      changed={this.namedChangedHandler}/>
      <Person 
      name={this.state.persons[1].name} 
      age={this.state.persons[1].age}
      click={this.switchNameHandler.bind(this,'Suzero')}>Hello, I'm inside of tag.</Person>
      <Person 
      name={this.state.persons[2].name} 
      age={this.state.persons[2].age}/> */}
      </div>);
    }
    const classes = [];
    if(this.state.persons.length<=2)
    {
      classes.push('red');
    }
    if(this.state.persons.length<=1)
    {
      classes.push('bold');
    }

    return(
      // <StyleRoot>
      <div className='App'>
        <h1>Hi, I'm React App.</h1>
        <p className={classes.join(' ')}>This is the testing paragraph</p>
      {persons}
      <StyledButton alt={this.state.showPersons} onClick={this.togglePersonsHandler}>Toggle Show</StyledButton>
      
      </div>
    );
  }

}






export default App;

/*import React, { useState } from 'react';
import './App.css';
import Person from './Person/Person';

const App =(props)=>{

  const [personsState, setPersonsState] = useState({
    persons:[
      {name:'Sunil',age:21},
      {name:'Ankit',age:23},
      {name:'Nitin',age:22}
    ] 
  });
  
const otherValue=useState('Some other value!');
console.log(otherValue);

 const switchNameHandler=()=>{
    setPersonsState({
      persons:[
      {name:'Sunil Panchal',age:18},
      {name:'Ankit Panchal',age:20},
      {name:'Nitin Panchal',age:19}
      ]
    })
  }

  return (
    <div className="App">
      <h1>Hi, I'm React App.</h1>
      <Person name={personsState.persons[0].name} age={personsState.persons[0].age}/>
      <Person name={personsState.persons[1].name} age={personsState.persons[1].age}>Hello, I'm inside of tag.</Person>
      <Person name={personsState.persons[2].name} age={personsState.persons[2].age}/>
      <button onClick={switchNameHandler}>Switch Name</button>
    </div>
    )
}

export default App;
*/